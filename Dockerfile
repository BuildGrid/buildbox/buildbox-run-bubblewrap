FROM registry.gitlab.com/buildgrid/buildbox/buildbox-worker:latest

RUN apt-get update \
    && apt-get install -y bubblewrap \
    && apt-get clean

COPY . /buildbox-run-bubblewrap

RUN cd /buildbox-run-bubblewrap && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=DEBUG .. && \
    make && \
    CTEST_OUTPUT_ON_FAILURE=1 make test

ENV PATH "/buildbox-run-bubblewrap/build:$PATH"

# Build Args to set default Server and CAS Server
ARG SERVER="http://127.0.0.1:50051"
ARG CAS_SERVER=${SERVER}

# Add as ENV (to use during runtime)
ENV BUILDGRID_SERVER_URL=${SERVER}
ENV CAS_SERVER_URL=${CAS_SERVER}

# Default entry point
CMD buildbox-worker --verbose --buildbox-run=buildbox-run-bubblewrap --bots-remote=${BUILDGRID_SERVER_URL} --cas-remote=${CAS_SERVER_URL}
