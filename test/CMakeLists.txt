find_file(BuildboxGTestSetup.cmake BuildboxGTestSetup.cmake HINTS ${BuildboxCommon_DIR})
include(${BuildboxGTestSetup.cmake})

set(target bubblewraprunner_tests)

add_executable(${target}
  bubblewrap.t.cpp
  buildboxrunbwrap_tests.m.cpp
  ../buildboxrun-bubblewrap/buildboxrun_bubblewrap.cpp
)

target_include_directories(${target} PRIVATE ../buildboxrun-bubblewrap)
target_link_libraries(${target} Buildbox::buildboxcommon ${GMOCK_TARGET})
add_test(NAME ${target} COMMAND ${target} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
