Transition notice
=================

This repository does not accept MRs anymore, as it is being merged into `buildbox`
per https://gitlab.com/BuildGrid/buildbox/buildbox-common/-/issues/92

buildbox-run-bubblewrap
=======================

An implementation of a runner for BuildGrid/buildbox using `Bubblewrap
<https://github.com/containers/bubblewrap>`_, a tool that allows
creating unprivileged chroot-like environments with Linux user
namespaces.

To run, ``bwrap`` is expected to be in ``$PATH``.

Installation
~~~~~~~~~~~~

If buildbox-common is not already installed, follow the installation instructions
for `buildbox-common <https://gitlab.com/BuildGrid/buildbox/buildbox-common>`_.

Ensure that you also have bubblewrap installed. In Debian/Ubuntu, this can be done
with::

    sudo apt install bubblewrap

If you have successfully installed buildbox-common, you should already have all
of the other dependencies required for buildbox-run-bubblewrap.

Assuming that you have downloaded the source for buildstream-run-bubblewrap,
you can install buildbox-run-bubblewrap by navigating to the
buildbox-run-bubblewrap folder, and running the following commands::

    cmake -DBUILD_TESTING=OFF -Bbuild
    make -C build
    sudo make -C build install


Usage
~~~~~
::

    usage: ./buildbox-run-bubblewrap [OPTIONS]
        --action=PATH               Path to read input Action from
        --action-result=PATH        Path to write output ActionResult to
        --log-level=LEVEL           (default: info) Log verbosity: trace/debug/info/warning/error
        --verbose                   Set log level to debug
        --log-file=FILE             File to write log to
        --use-localcas              Use LocalCAS protocol methods (default behavior)
                                    NOTE: this option will be deprecated.
        --disable-localcas          Do not use LocalCAS protocol methods
        --workspace-path=PATH       Location on disk which runner will use as root when executing jobs
        --stdout-file=FILE          File to redirect the command's stdout to
        --stderr-file=FILE          File to redirect the command's stderr to
        --no-logs-capture           Do not capture and upload the contents written to stdout and stderr
        --capabilities              Print capabilities supported by this runner
        --remote=URL                URL for CAS service
        --instance=NAME             Name of the CAS instance
        --server-cert=PATH          Public server certificate for TLS (PEM-encoded)
        --client-key=PATH           Private client key for TLS (PEM-encoded)
        --client-cert=PATH          Public client certificate for TLS (PEM-encoded)
        --retry-limit=INT           Number of times to retry on grpc errors
        --retry-delay=MILLISECONDS  How long to wait before the first grpc retry
        --bind-mount=HOSTPATH:PATH  Bind mount file or directory from host into sandbox
